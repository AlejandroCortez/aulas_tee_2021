// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAeEvZd0iDdEBRlhA7x8RxbgWd_AHz_VQ8',
    authDomain: 'controle-gu3006204.firebaseapp.com',
    projectId: 'controle-gu3006204',
    storageBucket: 'controle-gu3006204.appspot.com',
    messagingSenderId: '112211875888',
    appId: '1:112211875888:web:49f8a436bb10b509d6f5f5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
