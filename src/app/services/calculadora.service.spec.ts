import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('A Classe Calculadora', () => {
  let calculadora: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    calculadora = TestBed.inject(CalculadoraService);
  });

  //funcionalidade
  describe('deve realizar divisões', () => {

    //casos de teste
    it('entre número inteiros', () => {
      const result = calculadora.divide(8,4)
      expect(result).toBe(2);
    });

    it('com exceção do 0', () => {
      const result = calculadora.divide(8,0)
      expect(typeof result).toEqual('string');
    });
  });

  
});
